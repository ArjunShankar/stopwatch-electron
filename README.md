# Stopwatch

Electron app
1) npm install electron --save-dev
2) add main.js
3) update index.html
4) update package.json

Packaging
electron-packager . --platform=win32
electron-packager . --platform=darwin --overwrite
electron-packager . --overwrite --asar=true --platform=linux --arch=x64  --prune=true


https://angularfirebase.com/lessons/desktop-apps-with-electron-and-angular/
https://coursetro.com/posts/code/125/Angular-5-Electron-Tutorial
https://www.christianengvall.se/electron-app-icons/
https://www.christianengvall.se/electron-packager-tutorial/
