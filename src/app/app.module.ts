import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HmsPipe } from './hms.pipe';
import {MatCardModule, MatButtonModule, MatIconModule, MatInputModule, MatFormField} from '@angular/material';
import { StopwatchComponent } from './components/stopwatch/stopwatch.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HmsPipe,
    StopwatchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [StopwatchComponent]
})
export class AppModule { }
