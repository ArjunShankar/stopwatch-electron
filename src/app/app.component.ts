import {Component, ComponentFactoryResolver, ViewChild, ViewContainerRef} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';
import { Subscription } from 'rxjs/Subscription';
import {StopwatchComponent} from './components/stopwatch/stopwatch.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {

  constructor(private factoryResolver: ComponentFactoryResolver)  {

  }
  @ViewChild('container', {read: ViewContainerRef}) viewContainerRef: ViewContainerRef;


  add() {
    const factory = this.factoryResolver.resolveComponentFactory(StopwatchComponent);
    const component = factory.create(this.viewContainerRef.parentInjector);
    this.viewContainerRef.insert(component.hostView);
  }

}
